-- MySQL Script generated by MySQL Workbench
-- 06/23/18 12:05:47
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema facultet_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema facultet_db
-- -----------------------------------------------------
DROP DATABASE IF EXISTS `facultet_db`;

CREATE SCHEMA IF NOT EXISTS `facultet_db` DEFAULT CHARACTER SET utf8 ;
USE `facultet_db` ;

-- -----------------------------------------------------
-- Table `facultet_db`.`courses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facultet_db`.`courses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `theme` VARCHAR(45) NOT NULL,
  `start_date` DATE NOT NULL,
  `finish_date` DATE NOT NULL,
  `status_id` INT(11) NOT NULL,
  `student_count` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facultet_db`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facultet_db`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `fname` VARCHAR(45) NOT NULL,
  `lname` VARCHAR(45) NOT NULL,
  `role_id` INT(11) NULL DEFAULT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facultet_db`.`journals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facultet_db`.`journals` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `course_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `mark` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `FK_course_id_courses_id_idx` (`course_id` ASC),
  INDEX `FK_user_id_users_id_idx` (`user_id` ASC),
  CONSTRAINT `FKJ_course_id_courses_id`
    FOREIGN KEY (`course_id`)
    REFERENCES `facultet_db`.`courses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FKJ_user_id_users_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `facultet_db`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facultet_db`.`sign_course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facultet_db`.`sign_course` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `course_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `FK_user_id_users_id_idx` (`user_id` ASC),
  INDEX `FK_course_id_courses_id_idx` (`course_id` ASC),
  CONSTRAINT `FKS_course_id_courses_id`
    FOREIGN KEY (`course_id`)
    REFERENCES `facultet_db`.`courses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FKS_user_id_users_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `facultet_db`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facultet_db`.`teachers_course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facultet_db`.`teachers_course` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` INT(11) NOT NULL,
  `course_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `FK_teacher_id_user_id_idx` (`teacher_id` ASC),
  INDEX `FK_course_id_courses_id_idx` (`course_id` ASC),
  CONSTRAINT `FKT_course_id_courses_id`
    FOREIGN KEY (`course_id`)
    REFERENCES `facultet_db`.`courses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FKT_teacher_id_user_id`
    FOREIGN KEY (`teacher_id`)
    REFERENCES `facultet_db`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
