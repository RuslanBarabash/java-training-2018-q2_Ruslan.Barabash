<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-UTF-8">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<title>Main page</title>
<link rel="stylesheet" type="text/css" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" href="styles/styles.css" />


</head>
<body>

	<div class="wrapper" align="center">
		<%@ include file="/WEB-INF/jspf/header.jspf"%>

		<div class="context">
			<%@ include file="/WEB-INF/jspf/navigation.jspf"%>
			<div class="main_page">
				<h1>Добро пожаловать на сайт Best IT!</h1>
				<h2>Мы первые!</h2>
				<p>Мы открылись в 2015 году, чтобы помочь людям освоить новые
					профессии, а компаниям — найти грамотных специалистов. Мы начали
					работать первыми, поэтому многое пришлось придумывать с нуля. Мы
					разработали авторские методики преподавания и тесты по выбору
					направления. Придумали, как научить технарей выступать ярко.
					Внедрили практические занятия. И обустроили целый офисный этаж: с
					десятком лекториев, подвесными качелями и кофейней.</p>
					 <p><img src="images/our_team.jpg"></p>
			</div>

		</div>



		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>