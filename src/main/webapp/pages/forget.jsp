<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<title>Main page</title>
<link rel="stylesheet" type="text/css" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" href="styles/styles.css" />

</head>
<body>
	<div class="wrapper" align="center">
		<%@ include file="/WEB-INF/jspf/header.jspf"%>

		<div class="context">
			<%@ include file="/WEB-INF/jspf/navigation.jspf"%>

<c:out value="${OK}"></c:out>
			<div class="forget_block">
				<h1>Востановление пароля</h1>
				<form action="controller?command=forget_password" id="forget_form" method="post">
					<table id="autenth_table">
						
						<tr>
							<td>Email</td>
							<td><span>*</span></td>
							<td><input type="text" name="email" /></td>
						</tr>
					</table>
					<input type="submit" value="<fmt:message key="send"/>" id="forget_button">
				</form>
			</div>
		</div>
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>

</body>
</html>