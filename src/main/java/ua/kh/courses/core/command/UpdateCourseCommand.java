package ua.kh.courses.core.command;

import java.io.IOException;



import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kh.courses.core.db.dao.DAOFactory;
import ua.kh.courses.core.db.entity.Course;
import ua.kh.courses.core.exception.AppException;
import ua.kh.courses.core.util.DateUtils;

public class UpdateCourseCommand extends Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8675654074822811141L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		DAOFactory mysqlFactory = null;
		mysqlFactory=DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	Course course = new Course();
	course.setName(request.getParameter("course_name").toString());
	course.setTheme(request.getParameter("theme").toString());
	course.setStartDate(DateUtils.getDateFromString(request.getParameter("start_date").toString(), DateUtils.DATE_FORMAT));	
	course.setFinishDate(DateUtils.getDateFromString(request.getParameter("finish_date").toString(), DateUtils.DATE_FORMAT));	
	int id = Integer.parseInt(request.getParameter("id"));
	System.out.println("UPDATE");
	mysqlFactory.getCourseDAO().updateCourse(course,id);
	
		return "/controller?command=course_list&type=courses";
	}

}
