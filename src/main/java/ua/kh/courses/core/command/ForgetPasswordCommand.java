package ua.kh.courses.core.command;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kh.courses.core.db.dao.DAOFactory;
import ua.kh.courses.core.db.entity.User;
import ua.kh.courses.core.exception.AppException;
import ua.kh.courses.core.util.Utils;
import ua.kh.courses.path.Path;

public class ForgetPasswordCommand extends Command {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 164521797642404159L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		DAOFactory mysqlFactory = null;
		mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
		request.setAttribute("edit", true);

		String email = new String(request.getParameter("email").getBytes("ISO-8859-1"), "UTF-8");
		User user = mysqlFactory.getUserDAO().findUserByEmail(email);
		if ((user.getEmail() != null) || (!user.getEmail().isEmpty())) {
			Utils.sendMail(user.getEmail(), Utils.generateMessage(user.getLogin()));
		} else {
			request.setAttribute("errorMessage", new Error("There is no user with that name or email!"));

		}

		return Path.PAGE_INDEX;

	}
}
