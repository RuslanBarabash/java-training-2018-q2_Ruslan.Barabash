package ua.kh.courses.core.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kh.courses.core.exception.AppException;

public class AddTeacherMenu extends Command{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3122794515459541830L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		request.setAttribute("teacher_reg", true);
		return "controller?command=all_teachers";
	}

}
