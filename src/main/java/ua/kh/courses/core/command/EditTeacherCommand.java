package ua.kh.courses.core.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kh.courses.core.db.dao.DAOFactory;
import ua.kh.courses.core.db.entity.User;
import ua.kh.courses.core.exception.AppException;
import ua.kh.courses.path.Path;

public class EditTeacherCommand extends Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6043139337557052976L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		DAOFactory mysqlFactory = null;
		mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
		request.setAttribute("edit", true);
		int id = Integer.parseInt(request.getParameter("id"));
		User teacher;
		teacher = mysqlFactory.getUserDAO().findUserById(id);
		request.setAttribute("teacher_ed", teacher);
		return Path.ADMIN_CABINET;
	}

}
