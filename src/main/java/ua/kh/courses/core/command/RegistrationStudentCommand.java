package ua.kh.courses.core.command;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kh.courses.core.db.dao.DAOFactory;
import ua.kh.courses.core.db.entity.Role;
import ua.kh.courses.core.db.entity.User;
import ua.kh.courses.core.exception.AppException;
import ua.kh.courses.core.util.Utils;

public class RegistrationStudentCommand extends Command{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5004989891973601559L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
	User u = new User();
	DAOFactory mysqlFactory = null;
	mysqlFactory=DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	 request.setCharacterEncoding("UTF8");
u.setFname(request.getParameter("fname"));
u.setLname(request.getParameter("lname"));
u.setLogin(request.getParameter("login"));
u.setPassword(Utils.encrypt(request.getParameter("password")));
u.setEmail(request.getParameter("email"));
u.setRoleid(Role.STUDENT.ordinal());
	System.out.println(u.getFname());
	mysqlFactory.getUserDAO().insertUserIntoDatabase(u);
		return "/controller?command=about_courses";
	}

}
