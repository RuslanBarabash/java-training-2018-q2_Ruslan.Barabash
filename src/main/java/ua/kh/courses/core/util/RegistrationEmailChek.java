package ua.kh.courses.core.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ua.kh.courses.core.db.dao.sql.MysqlDAOFactory;
import ua.kh.courses.core.exception.DBException;

/**
 * Servlet implementation class RegistrationEmailChek
 */
@WebServlet("/registrationEmailChek")
public class RegistrationEmailChek extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		Statement st;
		ResultSet rs;
		String availEmail = request.getParameter("email");
		System.out.println(availEmail);
		String SQL = "SELECT email FROM users WHERE email='" + availEmail + "'";
		try {
			new MysqlDAOFactory();
			Connection con = MysqlDAOFactory.createConnection();
			try {
				st = con.createStatement();
				rs = st.executeQuery(SQL);
				if (rs.next()) {

					out.print("false");

					System.out.println("false");

				} else {

					out.print("true");

					System.out.println("true");

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		} catch (DBException e) {
			e.printStackTrace();
		} finally {
			out.close();
		}

	}

}
