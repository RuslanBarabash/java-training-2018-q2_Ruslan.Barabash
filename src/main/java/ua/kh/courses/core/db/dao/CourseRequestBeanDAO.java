package ua.kh.courses.core.db.dao;

import java.util.List;

import ua.kh.courses.core.db.bean.CourseRequestBean;
import ua.kh.courses.core.exception.DBException;

public interface CourseRequestBeanDAO {
	/** Find list of courses with full information. */
	List<CourseRequestBean> findAllCoursesBean() throws DBException ;
	/** Find courses by search parameters. */
	List<CourseRequestBean> findCoursesBySearchParametr(String parameter) throws DBException;
}
