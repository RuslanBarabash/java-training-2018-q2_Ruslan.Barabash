package ua.kh.courses.core.db.entity;

/**
 * Role entity.
 */
public enum Role {
	ADMIN, TEACHER, STUDENT, BUNNED;

	public static Role getRole(User user) {
		int roleId = user.getRoleid();
		return Role.values()[roleId];
	}

	public String getName() {
		return name().toLowerCase();
	}
}
