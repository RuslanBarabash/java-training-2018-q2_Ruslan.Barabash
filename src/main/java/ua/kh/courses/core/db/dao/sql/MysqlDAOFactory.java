package ua.kh.courses.core.db.dao.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import ua.kh.courses.core.db.dao.CourseDAO;
import ua.kh.courses.core.db.dao.CourseRequestBeanDAO;
import ua.kh.courses.core.db.dao.DAOFactory;
import ua.kh.courses.core.db.dao.JournalDAO;
import ua.kh.courses.core.db.dao.StudentOrderRequestDAO;
import ua.kh.courses.core.db.dao.TeacherRequestDAO;
import ua.kh.courses.core.db.dao.UserDAO;
import ua.kh.courses.core.exception.DBException;
import ua.kh.courses.core.exception.Messages;

/**
 * Extension of the abstract DAO-Factory-class for the MYSQL-Database.
 */
public class MysqlDAOFactory extends DAOFactory {
	private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/facultet_db?user=root&password=0000&useSSL=false";

	private static Connection con;

	/**
	 * Constructor MysqlDAOFactory.
	 */
	public MysqlDAOFactory() {
		super();
	}

	/** The logger. */
	private static final Logger LOG = Logger.getLogger(MysqlDAOFactory.class);

	/**
	 * Method to create a Connection on the mysql-database.
	 * Implementation  org.apache.tomcat.jdbc.pool 
	 * 
	 * @return the Connection.
	 * @throws DBException
	 */
	public static Connection createConnection() throws DBException {

		try {
			 
			 con = DriverManager.getConnection(CONNECTION_URL);
			 con.setAutoCommit(false);
			 LOG.trace("getConnection ==> " + con);
			} catch (SQLException ex) {
				LOG.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION, ex);
				throw new DBException(Messages.ERR_CANNOT_OBTAIN_CONNECTION, ex);
			}
		return con;
	}


	@Override
	public UserDAO getUserDAO() {
		return new SQLUserDAO();
	}

	@Override
	public TeacherRequestDAO getTeacherRequestDAO() {

		return new SQLTeacherRequestDAO();
	}

	@Override
	public StudentOrderRequestDAO getStudentOrderRequestDAO() {

		return new StudentOrderDAO();
	}

	@Override
	public JournalDAO getJournalDAO() {
		return new SQLJournalDAO();
	}

	@Override
	public CourseDAO getCourseDAO() {
		return new SQLCourseDAO();
	}

	@Override
	public CourseRequestBeanDAO getCourseRequestBeanDAO() {
		return new CourseRequestDAO();
	}
}
